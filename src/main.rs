extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate toml;
extern crate discord;

use std::fs::File;
use std::io::prelude::*;
use std::collections::HashMap;
use std::process::Command;
use std::error::Error;

use discord::{ Discord, State, model };

#[derive(Clone, Debug, Serialize, Deserialize)]
struct Config {
    token: String,
    commands: HashMap<String, String>
}

fn run(config: &Config) -> discord::Result<()> {
    let discord = Discord::from_bot_token(&config.token).unwrap();
    let (mut connection, ready_event) = discord.connect().unwrap();
    let mut state = State::new(ready_event);

    println!("connected");

    while let Ok(event) = connection.recv_event() {
        state.update(&event);
        if let model::Event::MessageCreate(message) = event {
            let mut words = message.content.split_whitespace();

            if message.author.id != state.user().id {
                if let Some(word) = words.next() {
                    if let Some(command) = config.commands.get(word) {
                        let mut args = command.split_whitespace().chain(words);
                        if let Some(command) = args.next() {
                            let output = String::from_utf8(
                                Command::new(command)
                                    .args(args)
                                    .output().unwrap().stdout
                            ).unwrap();
                            if !output.is_empty() {
                                discord.send_message(
                                    message.channel_id,
                                    &output,
                                    "",
                                    false
                                )?;
                            }
                        }
                    }

                    if message.content == "go away!" {
                        discord.send_message(message.channel_id, ":cry:", "", false).unwrap();
                        break;
                    }
                }
            }
        }
    }
    connection.shutdown()?;
    Ok(())
}

fn main() {
    let args: Vec<String> = std::env::args().collect();

    if args.len() > 1 {
        let config: Config = {
            let mut config_file = String::new();
            File::open(&args[1]).unwrap().read_to_string(&mut config_file).unwrap();
            toml::from_str(&config_file).unwrap()
        };

        while let Err(err) = run(&config) {
            eprintln!("Encountered error: {}", err.description());
            println!("Attempting to reconnect.")
        }
    } else {
        eprintln!("no config file argument provided!");
        std::process::exit(1);
    }
}
